$(document).ready(function () {
    // sliders
    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="ng69a" d="M1920 6302v-120l-45.93 49.4a15.81 15.81 0 0 0 0 21.2z"/><path id="ng69b" d="M1886.48 6241.24a8.28 8.28 0 0 1 4.1-1.24c3.55-.23 7.1-.57 10.65-.31l1.33-.09a59.6 59.6 0 0 1 6.65 0c-1.83-1.75-3.47-3.76-5.3-5.52-.13-.12-.13-.5-.05-.7.14-.39.5-.46.88-.4 1.26.21 2.38.9 3.34 1.79 1.87 1.71 3.78 3.38 5.5 5.26l.04.03a1.5 1.5 0 0 1-.03 2.05c-.44.44-.87.86-1.33 1.28l-.75.91a42.92 42.92 0 0 1-5.21 5.31c-.54.45-1.19.62-1.62.09-.4-.49-.36-1.19.08-1.77.8-1.03 1.6-2.09 2.51-3.02.91-.94 1.86-1.85 2.8-2.74-1.18.1-2.37.2-3.55.18-2.36-.03-4.7-.18-7.06-.28-4.22.89-8.54.38-12.8.56-.2 0-.5-.26-.6-.45-.18-.38.06-.7.42-.94z"/></defs><g><g transform="translate(-1867 -6180)"><use  xlink:href="#ng69a"/></g><g transform="translate(-1867 -6180)"><use fill="#fff" xlink:href="#ng69b"/></g></g></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="lsdqa" d="M0 6302v-120l45.93 49.4a15.81 15.81 0 0 1 0 21.2z"/><path id="lsdqb" d="M33.49 6241.24a8.26 8.26 0 0 0-4.1-1.24c-3.54-.23-7.08-.57-10.64-.31l-1.32-.09c-2.22-.1-4.44-.14-6.65 0 1.83-1.75 3.47-3.76 5.3-5.52.12-.12.12-.5.05-.7-.14-.39-.5-.46-.88-.4-1.26.21-2.38.9-3.34 1.79-1.86 1.71-3.77 3.38-5.5 5.26l-.03.03a1.5 1.5 0 0 0 .03 2.05c.43.44.87.86 1.32 1.28l.76.91a42.9 42.9 0 0 0 5.2 5.31c.54.45 1.18.62 1.61.09.4-.49.37-1.19-.08-1.77-.8-1.03-1.6-2.09-2.5-3.02a105 105 0 0 0-2.8-2.74c1.19.1 2.37.2 3.55.18 2.35-.03 4.7-.18 7.05-.28 4.22.89 8.53.38 12.8.56.18 0 .48-.26.58-.45.19-.38-.05-.7-.41-.94z"/></defs><g><g transform="translate(1 -6180)"><use  xlink:href="#lsdqa"/></g><g transform="translate(1 -6180)"><use fill="#fff" xlink:href="#lsdqb"/></g></g></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        appendArrows: '.slider-nav',
        dots: true,
        fade: true,
        cssEase: 'linear'
    });
    $('.slider-full-width').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="ng69a" d="M1920 6302v-120l-45.93 49.4a15.81 15.81 0 0 0 0 21.2z"/><path id="ng69b" d="M1886.48 6241.24a8.28 8.28 0 0 1 4.1-1.24c3.55-.23 7.1-.57 10.65-.31l1.33-.09a59.6 59.6 0 0 1 6.65 0c-1.83-1.75-3.47-3.76-5.3-5.52-.13-.12-.13-.5-.05-.7.14-.39.5-.46.88-.4 1.26.21 2.38.9 3.34 1.79 1.87 1.71 3.78 3.38 5.5 5.26l.04.03a1.5 1.5 0 0 1-.03 2.05c-.44.44-.87.86-1.33 1.28l-.75.91a42.92 42.92 0 0 1-5.21 5.31c-.54.45-1.19.62-1.62.09-.4-.49-.36-1.19.08-1.77.8-1.03 1.6-2.09 2.51-3.02.91-.94 1.86-1.85 2.8-2.74-1.18.1-2.37.2-3.55.18-2.36-.03-4.7-.18-7.06-.28-4.22.89-8.54.38-12.8.56-.2 0-.5-.26-.6-.45-.18-.38.06-.7.42-.94z"/></defs><g><g transform="translate(-1867 -6180)"><use  xlink:href="#ng69a"/></g><g transform="translate(-1867 -6180)"><use fill="#fff" xlink:href="#ng69b"/></g></g></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="lsdqa" d="M0 6302v-120l45.93 49.4a15.81 15.81 0 0 1 0 21.2z"/><path id="lsdqb" d="M33.49 6241.24a8.26 8.26 0 0 0-4.1-1.24c-3.54-.23-7.08-.57-10.64-.31l-1.32-.09c-2.22-.1-4.44-.14-6.65 0 1.83-1.75 3.47-3.76 5.3-5.52.12-.12.12-.5.05-.7-.14-.39-.5-.46-.88-.4-1.26.21-2.38.9-3.34 1.79-1.86 1.71-3.77 3.38-5.5 5.26l-.03.03a1.5 1.5 0 0 0 .03 2.05c.43.44.87.86 1.32 1.28l.76.91a42.9 42.9 0 0 0 5.2 5.31c.54.45 1.18.62 1.61.09.4-.49.37-1.19-.08-1.77-.8-1.03-1.6-2.09-2.5-3.02a105 105 0 0 0-2.8-2.74c1.19.1 2.37.2 3.55.18 2.35-.03 4.7-.18 7.05-.28 4.22.89 8.53.38 12.8.56.18 0 .48-.26.58-.45.19-.38-.05-.7-.41-.94z"/></defs><g><g transform="translate(1 -6180)"><use  xlink:href="#lsdqa"/></g><g transform="translate(1 -6180)"><use fill="#fff" xlink:href="#lsdqb"/></g></g></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        // slidesToShow: 5,
        // slidesToScroll: 1,
        arrows: true,
        rows: 2,
        slidesPerRow: 5,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    rows: 2,
                    slidesPerRow: 4
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    rows: 2,
                    slidesPerRow: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    rows: 2,
                    slidesPerRow: 2
                }
            },
            {
                breakpoint: 650,
                settings: {
                    rows: 2,
                    slidesPerRow: 1
                }
            }
        ]

    });
    $('.modal-slider-full-width').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="ng69a" d="M1920 6302v-120l-45.93 49.4a15.81 15.81 0 0 0 0 21.2z"/><path id="ng69b" d="M1886.48 6241.24a8.28 8.28 0 0 1 4.1-1.24c3.55-.23 7.1-.57 10.65-.31l1.33-.09a59.6 59.6 0 0 1 6.65 0c-1.83-1.75-3.47-3.76-5.3-5.52-.13-.12-.13-.5-.05-.7.14-.39.5-.46.88-.4 1.26.21 2.38.9 3.34 1.79 1.87 1.71 3.78 3.38 5.5 5.26l.04.03a1.5 1.5 0 0 1-.03 2.05c-.44.44-.87.86-1.33 1.28l-.75.91a42.92 42.92 0 0 1-5.21 5.31c-.54.45-1.19.62-1.62.09-.4-.49-.36-1.19.08-1.77.8-1.03 1.6-2.09 2.51-3.02.91-.94 1.86-1.85 2.8-2.74-1.18.1-2.37.2-3.55.18-2.36-.03-4.7-.18-7.06-.28-4.22.89-8.54.38-12.8.56-.2 0-.5-.26-.6-.45-.18-.38.06-.7.42-.94z"/></defs><g><g transform="translate(-1867 -6180)"><use  xlink:href="#ng69a"/></g><g transform="translate(-1867 -6180)"><use fill="#fff" xlink:href="#ng69b"/></g></g></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="lsdqa" d="M0 6302v-120l45.93 49.4a15.81 15.81 0 0 1 0 21.2z"/><path id="lsdqb" d="M33.49 6241.24a8.26 8.26 0 0 0-4.1-1.24c-3.54-.23-7.08-.57-10.64-.31l-1.32-.09c-2.22-.1-4.44-.14-6.65 0 1.83-1.75 3.47-3.76 5.3-5.52.12-.12.12-.5.05-.7-.14-.39-.5-.46-.88-.4-1.26.21-2.38.9-3.34 1.79-1.86 1.71-3.77 3.38-5.5 5.26l-.03.03a1.5 1.5 0 0 0 .03 2.05c.43.44.87.86 1.32 1.28l.76.91a42.9 42.9 0 0 0 5.2 5.31c.54.45 1.18.62 1.61.09.4-.49.37-1.19-.08-1.77-.8-1.03-1.6-2.09-2.5-3.02a105 105 0 0 0-2.8-2.74c1.19.1 2.37.2 3.55.18 2.35-.03 4.7-.18 7.05-.28 4.22.89 8.53.38 12.8.56.18 0 .48-.26.58-.45.19-.38-.05-.7-.41-.94z"/></defs><g><g transform="translate(1 -6180)"><use  xlink:href="#lsdqa"/></g><g transform="translate(1 -6180)"><use fill="#fff" xlink:href="#lsdqb"/></g></g></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        // slidesToShow: 5,
        // slidesToScroll: 1,
        arrows: true,
        rows: 2,
        slidesPerRow: 3,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    rows: 2,
                    slidesPerRow: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    rows: 2,
                    slidesPerRow: 1
                }
            }
        ]

    });
    $('.modal').on('shown.bs.modal', function (e) {

        $('.modal-slider-full-width').resize();
    });


    $(".modal-links a").click(function () {
        $(this).parents('.modal').addClass('modal-second-level');
    });

    $(".modal").on('hidden.bs.modal', function () {
        if ($(this).hasClass('modal-second-level')) {
            $("body").addClass("modal-open");
        }
    });


    $('.testemonials-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="ng69a" d="M1920 6302v-120l-45.93 49.4a15.81 15.81 0 0 0 0 21.2z"/><path id="ng69b" d="M1886.48 6241.24a8.28 8.28 0 0 1 4.1-1.24c3.55-.23 7.1-.57 10.65-.31l1.33-.09a59.6 59.6 0 0 1 6.65 0c-1.83-1.75-3.47-3.76-5.3-5.52-.13-.12-.13-.5-.05-.7.14-.39.5-.46.88-.4 1.26.21 2.38.9 3.34 1.79 1.87 1.71 3.78 3.38 5.5 5.26l.04.03a1.5 1.5 0 0 1-.03 2.05c-.44.44-.87.86-1.33 1.28l-.75.91a42.92 42.92 0 0 1-5.21 5.31c-.54.45-1.19.62-1.62.09-.4-.49-.36-1.19.08-1.77.8-1.03 1.6-2.09 2.51-3.02.91-.94 1.86-1.85 2.8-2.74-1.18.1-2.37.2-3.55.18-2.36-.03-4.7-.18-7.06-.28-4.22.89-8.54.38-12.8.56-.2 0-.5-.26-.6-.45-.18-.38.06-.7.42-.94z"/></defs><g><g transform="translate(-1867 -6180)"><use  xlink:href="#ng69a"/></g><g transform="translate(-1867 -6180)"><use fill="#fff" xlink:href="#ng69b"/></g></g></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="54" height="123" viewBox="0 0 54 123"><defs><path id="lsdqa" d="M0 6302v-120l45.93 49.4a15.81 15.81 0 0 1 0 21.2z"/><path id="lsdqb" d="M33.49 6241.24a8.26 8.26 0 0 0-4.1-1.24c-3.54-.23-7.08-.57-10.64-.31l-1.32-.09c-2.22-.1-4.44-.14-6.65 0 1.83-1.75 3.47-3.76 5.3-5.52.12-.12.12-.5.05-.7-.14-.39-.5-.46-.88-.4-1.26.21-2.38.9-3.34 1.79-1.86 1.71-3.77 3.38-5.5 5.26l-.03.03a1.5 1.5 0 0 0 .03 2.05c.43.44.87.86 1.32 1.28l.76.91a42.9 42.9 0 0 0 5.2 5.31c.54.45 1.18.62 1.61.09.4-.49.37-1.19-.08-1.77-.8-1.03-1.6-2.09-2.5-3.02a105 105 0 0 0-2.8-2.74c1.19.1 2.37.2 3.55.18 2.35-.03 4.7-.18 7.05-.28 4.22.89 8.53.38 12.8.56.18 0 .48-.26.58-.45.19-.38-.05-.7-.41-.94z"/></defs><g><g transform="translate(1 -6180)"><use  xlink:href="#lsdqa"/></g><g transform="translate(1 -6180)"><use fill="#fff" xlink:href="#lsdqb"/></g></g></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        appendArrows: '.jsTestemonialsSliderNav',
        dots: true,
        fade: true,
        cssEase: 'linear'
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        //>=, not <=
        if (scroll >= 500) {
            //clearHeader, not clearheader - caps H
            $("header").addClass("on-scroll");
        }
        else {
            $("header").removeClass("on-scroll");
        }
    }); //missing );

    // floating placeholder
    $("input, textarea").each(function (e) {
        $(this).wrap('<fieldset></fieldset>');
        var tag = $(this).attr("placeholder");
        //var tag= $(this).data("tag");
        $(this).attr("placeholder", "");
        $(this).after('<label for="name">' + tag + '</label>');
    });

    $('input, textarea').on('blur', function () {
        if (!$(this).val() == "") {
            $(this).next().addClass('stay');
        } else {
            $(this).next().removeClass('stay');
        }
    });

// // phone mask
    $(function () {
        $('[name="phone"]').mask("+7(000)000-00-00", {
            clearIfNotMatch: true,
            placeholder: "+7(___)___-__-__"
        });
        $('[name="phone"]').focus(function (e) {
            if ($('[name="phone"]').val().length == 0) {
                $(this).val('+7(');
            }
        })
    });


// slick slide same height
    var stHeight = $('.slider .slick-track').height();
    $('.slider .slick-slide').css('height', stHeight + 'px');

    // var stHeight = $('#feedbacks .slick-track').height();
    // $('#feedbacks .slick-slide').css('height',stHeight + 'px' );

// //form validation

    $('#orderForm .butn').click(function () {
        var parentClass = $(this).attr('rel');
        validate = 1;
        validate_msg = '';
        form = $('#' + $(this).attr('data-rel'));
        jQuery.each(form.find('.input-row .valid'), function (key, value) {

            if ($(this).val() == '') {
                validate_msg += $(this).attr('title') + '\n';
                validate = 0;
                $(this).focus();

                $(this).addClass('red-input');

                $(this).keyup(function () {

                    $(this).removeClass('red-input');

                });
            }

            else {
                $(this).removeClass('red-input');
            }
        });

        if (validate == 1) {
            $.ajax({
                url: 'send.php'
                , data: 'action=send_form&' + form.serialize()
                , success: function (data) {
                    $('form').trigger('reset');
                    // $('#call-back, #request').modal('hide');
                    $('#thanks').modal('show');
                }
            });
        }
        else {
        }
    });


    //equal hieght
    ;(function ($, window, document, undefined) {
        'use strict';

        var $list = $('.praz-service-wrap'),
            $items = $list.find('.praz-service'),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);
        $list.find('img').on('load', setHeights);
    })(jQuery, window, document);


});
